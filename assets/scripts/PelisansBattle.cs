﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PelisansBattle : MonoBehaviour
{
    public Slider slider;
    public Image fill;
    public GameObject pingui;
    public GameObject sangui;
    public GameObject leftbeak;
    public GameObject leftstartlaser;
    public GameObject rightstartlaser;
    public GameObject leftlaser;
    public GameObject rightlaser;
    public GameObject clashbeam;

    private float currentValue = 0f;
    private float sliderSpeed = 0.034f;
    private Color originalColor;
    private Animator pinguiAnim;
    private SpriteRenderer pinguiSpr;
    private SpriteRenderer lslSpr;
    private bool launch;
    private float dtime; 

    public float CurrentValue
    {
        get
        {
            return currentValue;
        }

        set
        {
            currentValue = value;
            slider.value = currentValue;
        }
    }

    private void Start()
    {
        CurrentValue = .5f;
        originalColor = fill.color;
        pinguiAnim = pingui.GetComponent<Animator>();
        pinguiAnim.Play("peliguin_jojo");
        pinguiSpr = pingui.GetComponent<SpriteRenderer>();
        leftbeak.GetComponent<SpriteRenderer>().sprite = null;
        //Destroy(leftbeak.GetComponent<Animator>());
        leftbeak.GetComponent<Animator>().enabled = false;
        lslSpr = leftstartlaser.GetComponent<SpriteRenderer>();
        lslSpr.sprite = null;
        leftstartlaser.GetComponent<Animator>().enabled = false;
        rightstartlaser.GetComponent<SpriteRenderer>().sprite = null;
        rightstartlaser.GetComponent<Animator>().enabled = false;
        launch = false;
    }

    private void Update()
    {
        if (slider.IsActive())
        {
            if (Input.GetButtonDown("Interact") && CurrentValue < 1)
            {
                CurrentValue += sliderSpeed;
            }
            else if (CurrentValue < 1 && CurrentValue > 0)
            {
                CurrentValue -= (sliderSpeed * Time.deltaTime) * 4;
            }
            else if (CurrentValue <= 0)
            {
                CurrentValue = 0;
            }
            else
            {
                fill.color = Color.Lerp(originalColor, Color.green, Mathf.PingPong(Time.time, 1));
                Invoke("nextlv", 2);
            }

        }

        if (launch && lslSpr.sprite.name == "laserpeligouin_7")
        {
            slider.gameObject.SetActive(true);

            leftlaser.GetComponent<SpriteRenderer>().enabled = true;
            rightlaser.GetComponent<SpriteRenderer>().enabled = true;
            clashbeam.GetComponent<SpriteRenderer>().enabled = true;

            Vector3 leftlaserScale = leftlaser.transform.localScale;
            leftlaserScale.x = CurrentValue * 32 * 8;
            leftlaser.transform.localScale = leftlaserScale;
            Vector3 rightlaserScale = rightlaser.transform.localScale; 
            rightlaserScale.x = (1-CurrentValue) * 32 * 8;
            rightlaser.transform.localScale = rightlaserScale;

            Vector3 clashbeamPosition = clashbeam.transform.localPosition;
            clashbeamPosition.x = 7.2f * (CurrentValue - 0.5f); //-3.6 à 3.6 (3.6*2=7.2) //CurrentValue : 0 à 1
            clashbeam.transform.localPosition = clashbeamPosition;
        }

        if (!launch && pinguiSpr.sprite.name == "jojoguin_2")
        {
            leftbeak.GetComponent<Animator>().enabled = true;
            leftstartlaser.GetComponent<Animator>().enabled = true;
            rightstartlaser.GetComponent<Animator>().enabled = true;

            launch = true;
        }


        //Debug.Log(pingui.GetComponent<SpriteRenderer>().sprite.name);
        //Debug.Log("Update : " + Time.deltaTime);
    }

    // private void FixedUpdate()
    // {
    //     if (slider.IsActive())
    //     {
    //         if (Input.GetButtonDown("Interact") && CurrentValue < 1)
    //         {
    //             CurrentValue += sliderSpeed;
    //         }
    //         else if (CurrentValue < 1 && CurrentValue > 0)
    //         {
    //             CurrentValue -= (sliderSpeed / 15);
    //         }
    //         else if (CurrentValue <= 0)
    //         {
    //             CurrentValue = 0;
    //         }
    //         else
    //         {
    //             fill.color = Color.Lerp(originalColor, Color.green, Mathf.PingPong(Time.time, 1));
    //             Invoke("nextlv", 2);
    //         }

    //     }
    //     Debug.Log("FixedUpdate : " + Time.deltaTime);
    // }

    private void nextlv() {
        LevelHandler.NextLevel();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public float speed = 20f;
    public Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb.velocity = transform.right * speed;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        Slime enemy = hitInfo.GetComponent<Slime>();
        if(enemy != null)
        {
            enemy.Die();
        }
        if(hitInfo.gameObject.tag != "ignoreCollider")
        {
            Destroy(gameObject);
        }
    }

}

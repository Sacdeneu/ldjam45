﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    //merci de pas toucher à cette putain de classe, cordialement.


    public Transform firePoint;
    public GameObject bulletPrefab;
    private PlayerInput PI;
    private InputFlags IF;
    // Start is called before the first frame update
    void Start()
    {
        PI = gameObject.GetComponent<PlayerInput>();
        IF = gameObject.GetComponent<InputFlags>();
    }

    // Update is called once per frame
    void Update()
    {
        if (PI.firePressed == true && IF.canShoot == true)
        {
            Shoot();
        }
    }

    void Shoot()
    {
        Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteract : MonoBehaviour
{
    public GameObject currentInterObj = null;
    private PlayerInput input;
    private Transform KeyPressedAnimation;
    private DialogueManager DM;
    void Start() {
        input = GetComponent<PlayerInput>();
        DM = GameObject.FindObjectOfType<DialogueManager>();
    }

    // Start is called before the first frame update
    void Update()
    {
        if(currentInterObj) {
            if(input.interactPressed && DM.dialogueFinished) {
                currentInterObj.SendMessage("DoInteraction");
            }
        }

        if(input.interactPressed && !DM.dialogueFinished) {
            DM.DisplayNextSentence();
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("interObject")){
            currentInterObj = other.gameObject;
            KeyPressedAnimation = currentInterObj.transform.Find("KeyPressed");
            KeyPressedAnimation.gameObject.SetActive(true);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("interObject"))
        {
            if(other.gameObject == currentInterObj)
            {
                KeyPressedAnimation.gameObject.SetActive(false);
                KeyPressedAnimation = null;
                currentInterObj = null;
            }
        }
    }
}

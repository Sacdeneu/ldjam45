﻿// This script handles inputs for the player. It serves two main purposes: 1) wrap up
// inputs so swapping between mobile and standalone is simpler and 2) keeping inputs
// from Update() in sync with FixedUpdate()

using UnityEngine;

//We first ensure this script runs before all other player scripts to prevent laggy
[DefaultExecutionOrder(-100)]
public class PlayerInput : MonoBehaviour {

    [HideInInspector] public float horizontal;
    // float that stores horizontal input
    [HideInInspector] public bool jumpPressed;
    // bool that stores jump held
    [HideInInspector] public bool interactPressed;

    [HideInInspector] public bool firePressed;

    private bool readyToClear;
    // bool used to keep input in sync

    void Update() {
        // Clear out existing input values
        ClearInput();

        // Process keyboard, mouse, gamepad (etc) inputs
        ProcessInputs();

        // Clamp the horizontal input to be between -1 and 1
        horizontal = Mathf.Clamp(horizontal, -1f, 1f);
    }

    void FixedUpdate() {
        //In FixedUpdate() we set a flag that lets inputs to be cleared out during the 
        //next Update(). This ensures that all code gets to use the current inputs
        readyToClear = true;
    }

    void ClearInput() {
        // If we're not ready to clear input, exit
        if (!readyToClear)
            return;

        //Reset all inputs
        horizontal = 0f;
        jumpPressed = false;
        interactPressed = false;
        firePressed = false;

        readyToClear = false;
    }

    void ProcessInputs() {
        // Accumulate horizontal axis input
        horizontal += Input.GetAxis("Horizontal");

        // Accumulate button inputs
        jumpPressed = jumpPressed || Input.GetButtonDown("Jump");
        interactPressed = interactPressed || Input.GetButtonDown("Interact");
        firePressed = firePressed || Input.GetButtonDown("Fire");
    }

}

﻿// This script controls the player's movement and physics within the game

using UnityEngine;

public class PlayerMovement : MonoBehaviour {
// Public properties
    [Header("Movement Properties")]
    public float speed = 8f;                // Player speed
    public float maxFallSpeed = -25f;       // Max speed player can fall

    [Header("Jump Properties")]
    public float jumpForce = 6.3f;          // Initial force of jump

    [Header("Environment Check Properties")]
    public LayerMask groundLayer;           // Layer of the ground

    [Header("Status Flags")]
    public bool isOnGround;                 //Is the player on the ground?
    public bool isJumping;                  //Is player jumping?
	public Animator animator;               //Animator to select appropriate animation

// Private properties
    private InputFlags flags;
    private PlayerInput input;                      // The current inputs for the player
    private BoxCollider2D bodyCollider;             // The collider component
    private Rigidbody2D rigidBody;                  // The rigidbody component
    private GameObject Firepoint;

    private float originalXScale;                   //Original scale on X axis
    private int direction = 1;                      // Direction player is facing

    void Start() {
        // Get a reference to the required components
        flags = GetComponent<InputFlags>();
        input = GetComponent<PlayerInput>();
        rigidBody = GetComponent<Rigidbody2D>();
        bodyCollider = GetComponent<BoxCollider2D>();
        Firepoint = GameObject.Find("Firepoint");

        // Record the original x scale of the player
        originalXScale = transform.localScale.x;
    }

    void FixedUpdate() {
        // Check the environment to determine status
        PhysicsCheck();

        // Process ground and air movements
        if(rigidBody.bodyType != RigidbodyType2D.Static) {
            GroundMovement();
            MidAirMovement();
            animator.SetFloat("Speed", Mathf.Abs(input.horizontal));
        } else {
            animator.SetFloat("Speed", 0);
        }

    }

    void PhysicsCheck() {
        // Start by assuming the player isn't on the ground and the head isn't blocked
        isOnGround = false;

        // Cast rays for the left and right foot
        float gd = bodyCollider.size.y / 2 - bodyCollider.offset.y + 0.05f;
        RaycastHit2D checkDownHit = Raycast(Vector2.down, gd);

        // If either ray hit the ground, the player is on the ground
        if (checkDownHit)
            isOnGround = true;

        if(isJumping && isOnGround) {
            isJumping = false;
        }
    }

    void GroundMovement() {
        // Calculate the desired velocity based on inputs
        float xVelocity = speed * input.horizontal;

        // If the sign of the velocity and direction don't match, flip the character
        if (xVelocity * direction < 0f)
            FlipCharacterDirection();

        // Check if char hits something on his right => enhance this before use
        // float jpp = bodyCollider.size.x / 2 - bodyCollider.offset.x + 0.05f;
        // RaycastHit2D rightCheck = Raycast(Vector2.right, jpp);

        // Apply the desired velocity
        moveRightOrLeft(xVelocity);
    }

    void MidAirMovement() {
        // If the jump key is pressed AND the player isn't already jumping AND EITHER
        // the player is on the ground or within the coyote time window...
        if (input.jumpPressed && !isJumping) {
            jump();
        }

        // If player is falling too fast, reduce the Y velocity to the max
        if (rigidBody.velocity.y < maxFallSpeed)
            rigidBody.velocity = new Vector2(rigidBody.velocity.x, maxFallSpeed);
    }

    void FlipCharacterDirection() {
        // Turn the character by flipping the direction
        direction *= -1;

        // Record the current scale
        Vector3 scale = transform.localScale;

        // Set the X scale to be the original times the direction
        scale.x = originalXScale * direction;

        // Apply the new scale
        transform.localScale = scale;

        //Rotate firepoint
        Firepoint.transform.Rotate(0f, 180f, 0f);
    }
    private void moveRightOrLeft(float xVelocity) {
        if(xVelocity > 0 && !flags.canGoRight)
            return;
        if(xVelocity < 0 && !flags.canGoLeft)
            return;

        rigidBody.velocity = new Vector2(xVelocity, rigidBody.velocity.y);
        // if(flags.cangoRight || flags.cangoRight) {
        //     AudioManager.PlayWalkAudio();
        // } else {
        //     AudioManager.PlayFailJump();
        // }
    }

    private void jump() {
        if(flags.canJump) {
            // ...The player is no longer on the groud and is jumping...
            isOnGround = false;
            isJumping = true;

            // ...add the jump force to the rigidbody...
            rigidBody.AddForce(new Vector2(0f, jumpForce), ForceMode2D.Impulse);

            // AudioManager.PlayJumpAudio();
        }else {
            // AudioManager.PlayFailJump();
        }
    }

    // These two Raycast methods wrap the Physics2D.Raycast() and provide some extra
    // functionality
    RaycastHit2D Raycast(Vector2 rayDirection, float length)
    {
        // Call the overloaded Raycast() method using the ground layermask
        // and return the results
        return Raycast(rayDirection, length, groundLayer);
    }

    RaycastHit2D Raycast(Vector2 rayDirection, float length, LayerMask mask)
    {
        // Record the player's position
        Vector2 pos = transform.position;

        // Send out the desired raycast and record the result
        RaycastHit2D hit = Physics2D.Raycast(pos, rayDirection, length, mask);

        Color color = hit ? Color.red : Color.green;
        //...and draw the ray in the scene view
        Debug.DrawRay(pos, rayDirection * length, color);

        // Return the results of the raycast
        return hit;
    }
}

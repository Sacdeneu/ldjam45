﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
public class DialogueManager : MonoBehaviour
{
    private Queue<string> sentences;

    public Animator animator;
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI dialogueText;

    public TextMeshProUGUI nextText;

    void Start()
    {
        sentences = new Queue<string>();
    }

    DialogueTrigger.DelegateFunction callback;

    public void StartDialogue(Dialogue dialogue, DialogueTrigger.DelegateFunction callback = null)
    {
        this.callback = callback;
        animator.SetBool("IsOpen", true);
        nameText.text = dialogue.name;

        sentences.Clear();

        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }
        DisplayNextSentence();
    }

    public bool isWriting = false;
    public bool dialogueFinished = true;
    private bool skip = false;
    public void DisplayNextSentence()
    {
        dialogueFinished = false;
        if (sentences.Count == 0 && !isWriting)
        {
            EndDialogue();
            dialogueFinished = true;
            callback?.Invoke();
            return;
        }

        if(!isWriting) {
            string sentence = sentences.Dequeue();
            StopAllCoroutines();
            StartCoroutine(TypeSentence(sentence));
        } else {
            skip = true;
        }
    }

    IEnumerator TypeSentence (string sentence)
    {
        isWriting = true;
        nextText.text = "Skip";

        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            if(skip) {
                dialogueText.text = sentence;
                skip = false;
                break;
            }
            dialogueText.text += letter;
            yield return null;
        }

        nextText.text = (sentences.Count == 0 ? "Close" : "Continue");
        isWriting = false;
    }

    public void EndDialogue()
    {
        animator.SetBool("IsOpen", false);
        Debug.Log("End of conversation");
    }
}

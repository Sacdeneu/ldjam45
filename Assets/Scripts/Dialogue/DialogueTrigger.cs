﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    public Dialogue dialogue;

    // private void OnTriggerEnter2D(Collider2D collision)
    // {
    //     Debug.Log(collision.name);
    //     TriggerDialogue();
    // }

    public delegate void DelegateFunction(); //callback

    private void defaultCB() {
        GameObject.FindGameObjectWithTag("Player")
            .gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
    }

    public void TriggerDialogue(DelegateFunction callback = null)
    {
        if(callback == null) {
            callback = defaultCB;
            GameObject.FindGameObjectWithTag("Player")
            .gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        }
        FindObjectOfType<DialogueManager>().StartDialogue(dialogue, callback);
    }
}

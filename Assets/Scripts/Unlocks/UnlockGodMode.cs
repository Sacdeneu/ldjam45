﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockGodMode : Unlock {
    override protected void action() {
        obj.GetComponent<InputFlags>().canShoot = true;
    }
}

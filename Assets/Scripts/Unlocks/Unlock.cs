﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Unlock : MonoBehaviour {
    public GameObject obj;
    public DialogueTrigger dialogue = null;

    protected abstract void action();

    private void destroyPlayer() {
        obj.SetActive(false);
    }
    private void cb() {
        action();
        destroyPlayer();
        LevelHandler.NextLevel();
    }
    public void unlock() {
        if(dialogue != null)
            dialogue.TriggerDialogue(cb);
    }
}

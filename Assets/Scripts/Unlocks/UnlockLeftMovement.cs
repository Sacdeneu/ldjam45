﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockLeftMovement : Unlock
{
    protected override void action()
    {
        obj.GetComponent<InputFlags>().canGoLeft = true;
    }
}

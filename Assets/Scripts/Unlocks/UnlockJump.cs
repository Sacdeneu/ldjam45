﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockJump : Unlock {
    override protected void action() {
        obj.GetComponent<InputFlags>().canJump = true;
    }
}

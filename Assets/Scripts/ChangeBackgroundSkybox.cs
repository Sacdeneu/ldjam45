﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeBackgroundSkybox : MonoBehaviour
{
    public Camera camera;
    public float duration;
    Color color1;
    Color color2;
    public Color coloradd2;
    bool isActivated = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isActivated)
        {
            //CHANGE BACKGROUND COLOR OF CAMERA
            color1 = Color.black;

            color2 = coloradd2;

            camera.GetComponent<Camera>().clearFlags = CameraClearFlags.SolidColor;
            float t = Mathf.PingPong(Time.time, duration) / duration;
            camera.GetComponent<Camera>().backgroundColor = Color.Lerp(color1, color2, t);
        }
       }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            isActivated = true;
        }
    }
}

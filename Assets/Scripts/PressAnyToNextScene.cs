﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressAnyToNextScene : MonoBehaviour {
    void Update() {
        if(Input.GetButtonDown("Interact")
            || Input.GetButtonUp("Interact")
            || Input.GetButton("Interact"))
            return;
        if(Input.anyKey) {
            LevelHandler.NextLevel();
        }
    }
}

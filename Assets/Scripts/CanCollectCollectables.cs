﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanCollectCollectables : MonoBehaviour {
    void Start() {
        
    }

    void Update() {
        
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.CompareTag("Collectable")) {
            other.gameObject.SetActive(false);
            // Unlock the stuff
            gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
            other.gameObject.GetComponent<Unlock>().unlock();
        }
    }
}

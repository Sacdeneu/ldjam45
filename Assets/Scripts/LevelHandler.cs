﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class LevelHandler
{
    private static string[] allScenes = new string[]
        {"MenuScene", "Intro_Level", "1_Level", "2_Level", "3_Level",
            "4_Level", "5_Level", "Sans_Battle", "Final_Level" };
    private static int currentLevel = 0;
    public static void LoadLevel(string levelname)
    {
        //Load a scene with his scene name
        SceneManager.LoadScene(levelname);
    }

    public static void NextLevel() {
        if(currentLevel == allScenes.Length - 1)
            currentLevel = 0;
        else
            currentLevel++;
        LoadLevel(allScenes[currentLevel]);
    }
}

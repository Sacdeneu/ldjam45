﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    // Launch First Scene
    public void PlayGame()
    {
        LevelHandler.NextLevel();
    }

    // Quit the gale
    public void QuitGame()
    {
        Application.Quit();
    }

}

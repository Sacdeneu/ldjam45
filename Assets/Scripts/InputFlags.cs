﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputFlags : MonoBehaviour {
    public bool canJump = false;
    public bool canGoLeft = true;
    public bool canGoRight = true;
    public bool canShoot = false;
}

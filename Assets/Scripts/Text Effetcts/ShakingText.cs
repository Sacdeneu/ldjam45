﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShakingText : MonoBehaviour {

    private TextMeshProUGUI tmm;

    // Start is called before the first frame update
    void Start() {
        tmm = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update() {
        // Petit test, pour l'instant c nul xD
        int rx = Random.Range(-15, 15);
        int ry = Random.Range(-15, 15);

        gameObject.transform.Translate(new Vector2(rx, ry));
    }
}
